﻿namespace PerformBoarding
{
    class FlightAtendant : Traveler
    {
        public override TravelerType GetTravelerType() => TravelerType.FLIGHT_ATENDANT;

        public override string ToString() => "-Comissario-";
        public override bool CanDriveTheShuttle() => false;
        public override bool CanStayAlone() => true;
        public override bool CanStayAloneWith(Traveler traveler) => !traveler.GetTravelerType().Equals(TravelerType.PILOT) &&
                                                                    !traveler.GetTravelerType().Equals(TravelerType.PRISIONER);

    }
}
