﻿namespace PerformBoarding
{
    class HeadFlightAtendant : Traveler
    {
        public override TravelerType GetTravelerType() => TravelerType.HEAD_FLIGHT_ATENDANT;

        public override string ToString() => "-Chefe de Comissario-";
        public override bool CanDriveTheShuttle() => true;
        public override bool CanStayAlone() => true;
        public override bool CanStayAloneWith(Traveler traveler) => !traveler.GetTravelerType().Equals(TravelerType.OFFICER) &&
                                                                    !traveler.GetTravelerType().Equals(TravelerType.PRISIONER);

    }
}
