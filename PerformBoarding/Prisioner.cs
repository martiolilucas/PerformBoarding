﻿namespace PerformBoarding
{
    class Prisioner : Traveler
    {
        public override TravelerType GetTravelerType() => TravelerType.PRISIONER;

        public override string ToString() => "-Prisioneiro-";
        public override bool CanDriveTheShuttle() => false;
        public override bool CanStayAlone() => false;
        public override bool CanStayAloneWith(Traveler traveler) => traveler.GetTravelerType().Equals(TravelerType.POLICE_OFFICER);

    }
}
