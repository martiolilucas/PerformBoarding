﻿using System;

namespace PerformBoarding
{

    class Program
    {
        static void Main(string[] args)
        {

            var boardingGame = new BoardingGame();

            boardingGame.Add(new Pilot());
            boardingGame.Add(new Officer());
            boardingGame.Add(new Officer());
            boardingGame.Add(new HeadFlightAtendant());
            boardingGame.Add(new FlightAtendant());
            boardingGame.Add(new FlightAtendant());
            boardingGame.Add(new PoliceOfficer());
            boardingGame.Add(new Prisioner());

            Console.WriteLine("Iniciando embarque...");
            try
            {
                boardingGame.PlayAlone();
                Console.WriteLine(boardingGame.GetResult());
                Console.WriteLine("Embarque finalizado, Boa Viagem!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }


            Console.WriteLine("Pressione qualquer tecla para finalizar...");
            Console.ReadKey();
        }


    }
}
