﻿using System;

namespace PerformBoarding
{
    enum TravelerType
    {
        POLICE_OFFICER,
        PRISIONER,
        PILOT,
        OFFICER,
        HEAD_FLIGHT_ATENDANT,
        FLIGHT_ATENDANT
    }

    abstract class Traveler
    {
        public Traveler()
        {
            PassportID = Guid.NewGuid();
        }

        public Guid PassportID { get; private set; }
        public bool Equals(Traveler traveler) => traveler.PassportID.Equals(PassportID);

        public abstract TravelerType GetTravelerType();
        public abstract bool CanStayAlone();
        public abstract bool CanStayAloneWith(Traveler traveler);
        public abstract bool CanDriveTheShuttle();



    }
}
