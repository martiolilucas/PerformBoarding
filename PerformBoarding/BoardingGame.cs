﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PerformBoarding
{
    class BoardingGame
    {
        private readonly List<Traveler> _onBoard = new List<Traveler>();
        private readonly List<Traveler> _inAirportTerminal = new List<Traveler>();
        private readonly Stack<ShuttleRun> _shuttleRunLog = new Stack<ShuttleRun>();
        private readonly Stack<Tuple<string, string>> _travelersLocationLog = new Stack<Tuple<string, string>>();

        private List<Traveler> OnBoard => _onBoard;
        private List<Traveler> InAirportTerminal => _inAirportTerminal;
        private Stack<ShuttleRun> ShuttleRunLog => _shuttleRunLog;
        private Stack<Tuple<string, string>> TravelersLocationLog => _travelersLocationLog;

        private Shuttle Shuttle { get; set; } = new Shuttle(ShuttleLocation.AIRPORT_TERMINAL);

        private void LogShuttleRun(ShuttleRunDirection runDirection, Traveler[] travelers) => ShuttleRunLog.Push(new ShuttleRun(runDirection, travelers));
        private void LogTravelersLocation() => TravelersLocationLog.Push(new Tuple<string, string>(TravelersToString("Terminal", InAirportTerminal), TravelersToString("Avião", OnBoard)));

        private bool IsValidState() => IsAmbienceStateValid(OnBoard) && IsAmbienceStateValid(InAirportTerminal);

        public void Add(Traveler traveler) => InAirportTerminal.Add(traveler);

        public string GetResult()
        {
            string result = "";

            while (ShuttleRunLog.Count > 0)
                result = "\nSTEP " + ShuttleRunLog.Count + "\n" +
                         ShuttleRunLog.Pop() + "\n" +
                         TravelersLocationLog.Peek().Item1 + "\n" +
                         TravelersLocationLog.Pop().Item2 + "\n" +
                         result;

            return result;

        }

        public void PlayAlone()
        {
            if (InAirportTerminal.Count <= 0)
                throw new InvalidOperationException("O Jogo não pode ser jogado sem nenhum personagem! Primeiro adicione os personagens!");

            if (!InAirportTerminal.Exists(t => t.CanDriveTheShuttle()))
                throw new Exception("Ninguém pode dirigir o Smart Fortwo. O embarque não pode ser efetuado!");

            if (!IsValidState())
                throw new Exception("O estado inicial do jogo não respeita as regras. Reveja!");

            while (InAirportTerminal.Count > 0)
                MovmentTravelers();
        }

        private void MovmentTravelers()
        {
            if (Shuttle.ShuttleIsInAirportTerminalLocation())
                GoWithAPassager(InAirportTerminal);
            else
                GoAlone(OnBoard);
        }

        private string TravelersToString(string name, List<Traveler> travelers)
        {
            string str = name + " { ";

            travelers.ForEach(t => str += t + ", ");

            if (travelers.Count > 0)
                str = str.Remove(str.Length - 2);

            str += " }";

            return str;
        }

        private bool IsAmbienceStateValid(List<Traveler> ambience)
        {

            if (ambience.Count == 0)
                return true;

            if (ambience.Count > 2)
                return !ambience.Exists(t => t.GetTravelerType().Equals(TravelerType.PRISIONER)) ||
                        ambience.Exists(t => t.GetTravelerType().Equals(TravelerType.POLICE_OFFICER));

            if (ambience.Count == 2)
                return ambience[0].CanStayAloneWith(ambience[1]);

            return ambience.First().CanStayAlone();
        }


        private void FromPlaneToAirportTerminal(params Traveler[] travelers)
        {
            foreach (var t in travelers)
            {
                OnBoard.Remove(t);
                InAirportTerminal.Add(t);
            }

            LogShuttleRun(ShuttleRunDirection.TOWARDS_AIRPORT, travelers);
            LogTravelersLocation();
            Shuttle.GoesTo(ShuttleLocation.AIRPORT_TERMINAL);

        }

        private void FromAirportTerminalToPlane(params Traveler[] travelers)
        {
            foreach (var t in travelers)
            {
                InAirportTerminal.Remove(t);
                OnBoard.Add(t);
            }


            LogShuttleRun(ShuttleRunDirection.TOWARDS_PLANE, travelers);
            LogTravelersLocation();
            Shuttle.GoesTo(ShuttleLocation.PLANE);
        }

        private void RevertLastMovment()
        {

            if (ShuttleRunLog.Count == 0)
                return;

            var shuttleRun = ShuttleRunLog.Pop();

            if (shuttleRun.Direction == ShuttleRunDirection.TOWARDS_AIRPORT)
            {
                Shuttle.GoesTo(ShuttleLocation.PLANE);
                foreach (var t in shuttleRun.Travelers)
                {
                    OnBoard.Add(t);
                    InAirportTerminal.Remove(t);
                }
            }
            else
            {
                Shuttle.GoesTo(ShuttleLocation.AIRPORT_TERMINAL);
                foreach (var t in shuttleRun.Travelers)
                {
                    OnBoard.Remove(t);
                    InAirportTerminal.Add(t);
                }
            }

            TravelersLocationLog.Pop();
        }

        private void GoWithAPassager(List<Traveler> ambience)
        {
            List<Traveler> availableDrivers;
            List<Traveler> availablePassagers;

            availableDrivers = ambience.FindAll(t => t.CanDriveTheShuttle());

            foreach (var driver in availableDrivers)
            {
                availablePassagers = ambience.FindAll(t => !t.Equals(driver) && t.CanStayAloneWith(driver));
                foreach (var passanger in availablePassagers)
                {
                    FromAirportTerminalToPlane(driver, passanger);
                    if (IsValidState())
                        return;

                    RevertLastMovment();
                }
            }

            RevertLastMovment();
        }

        private void GoAlone(List<Traveler> ambience)
        {
            List<Traveler> availableDrivers;
            availableDrivers = ambience.FindAll(t => t.CanDriveTheShuttle());

            foreach (var driver in availableDrivers)
            {
                FromPlaneToAirportTerminal(driver);
                if (IsValidState())
                    return;

                RevertLastMovment();
            }

            RevertLastMovment();
        }

    }
}
