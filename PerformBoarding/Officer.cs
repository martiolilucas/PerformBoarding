﻿namespace PerformBoarding
{
    class Officer : Traveler
    {
        public override TravelerType GetTravelerType() => PerformBoarding.TravelerType.OFFICER;

        public override string ToString() => "-Oficial-";
        public override bool CanDriveTheShuttle() => false;
        public override bool CanStayAlone() => true;
        public override bool CanStayAloneWith(Traveler traveler) => !traveler.GetTravelerType().Equals(TravelerType.HEAD_FLIGHT_ATENDANT) &&
                                                                    !traveler.GetTravelerType().Equals(TravelerType.PRISIONER);

    }
}
