﻿using System.Collections.Generic;

namespace PerformBoarding
{

    enum ShuttleRunDirection
    {
        TOWARDS_AIRPORT,
        TOWARDS_PLANE
    }

    class ShuttleRun
    {
        private List<Traveler> _travelers;

        public ShuttleRunDirection Direction { get; private set; }
        public List<Traveler> Travelers => _travelers;

        public ShuttleRun(ShuttleRunDirection direction, params Traveler[] travelers)
        {
            Direction = direction;
            _travelers = new List<Traveler>(travelers);
        }

        public override string ToString()
        {
            string direction = Direction.Equals(ShuttleRunDirection.TOWARDS_AIRPORT) ? "AEROPORTO" : "AVIAO";

            string travelers = "";

            foreach (var t in _travelers)
            {
                travelers += t + ", ";
            }

            return travelers + " PARA O " + direction + "\n";
        }

    }
}
