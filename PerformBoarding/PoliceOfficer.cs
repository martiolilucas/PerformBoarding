﻿namespace PerformBoarding
{
    class PoliceOfficer : Traveler
    {
        public override TravelerType GetTravelerType() => TravelerType.POLICE_OFFICER;

        public override string ToString() => "-Policial-";
        public override bool CanDriveTheShuttle() => true;
        public override bool CanStayAlone() => true;
        public override bool CanStayAloneWith(Traveler traveler) => true;
    }
}
