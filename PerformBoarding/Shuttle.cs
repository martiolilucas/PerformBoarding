﻿namespace PerformBoarding
{

    enum ShuttleLocation
    {
        PLANE,
        AIRPORT_TERMINAL
    }

    class Shuttle
    {

        public Shuttle(ShuttleLocation shuttleLocation)
        {
            ShuttleLocation = shuttleLocation;
        }

        private ShuttleLocation ShuttleLocation { get; set; }


        public bool ShuttleIsInPlaneLocation() => ShuttleLocation.Equals(ShuttleLocation.PLANE);
        public bool ShuttleIsInAirportTerminalLocation() => ShuttleLocation.Equals(ShuttleLocation.AIRPORT_TERMINAL);


        public void GoesTo(ShuttleLocation shuttleLocation)
        {
            ShuttleLocation = shuttleLocation;
        }

    }
}
