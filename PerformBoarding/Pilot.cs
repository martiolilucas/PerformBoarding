﻿namespace PerformBoarding
{
    class Pilot : Traveler
    {
        public override TravelerType GetTravelerType() => TravelerType.PILOT;

        public override string ToString() => "-Piloto-";
        public override bool CanDriveTheShuttle() => true;
        public override bool CanStayAlone() => true;
        public override bool CanStayAloneWith(Traveler traveler) => !traveler.GetTravelerType().Equals(TravelerType.FLIGHT_ATENDANT) &&
                                                                    !traveler.GetTravelerType().Equals(TravelerType.PRISIONER);

    }
}
